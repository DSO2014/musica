DEBUG	= -O3
CC	= gcc
INCLUDE	= -I/usr/local/include
CFLAGS	= $(DEBUG) -Wall $(INCLUDE) -Winline -pipe

LDFLAGS	= -L/usr/local/lib
LDLIBS    = -lwiringPi -lwiringPiDev -lpthread -lm

SRC	=	musica.c

OBJ	=	$(SRC:.c=.o)

BINS	=	$(SRC:.c=)

all:	$(BINS)

real-time-on:
	sudo sh -c "echo -1 > /proc/sys/kernel/sched_rt_runtime_us"

real-time-off:
	sudo sh -c "echo 950000 > /proc/sys/kernel/sched_rt_runtime_us"

musica:	musica.c
	@echo [compile]
	$(CC) -o $@ $< $(LDFLAGS) $(LDLIBS)

clean:
	@echo "[Clean]"
	@rm -f $(OBJ) $(BINS)

